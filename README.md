# sber_housing_market

## Краткое описание
Проверка влияния различных способов подготовки входных данных на качество работы регрессионной модели на примере задачи прогнозирования цены на объекты недвижимости в Москве.

## Язык реализации
Python 3.8.10
## Используемые сторонние модули
* numpy==1.21.2
* pandas==1.3.3
* scikit-learn==1.0
* plotly==5.3.1

## Начало работы
Установить необходимые модули
    !sudo pip3 install -r requirenments.txt
В директории проекта sber_housing_market необходимо создать следующую структуру папок
* sber_housin_market 
 + notebooks
 + data
    - raw
    - results

После этого поместить файл affection_of_features_preparation.ipynb в папку notebooks. Также необходимо скачать данные с https://www.kaggle.com/c/sberbank-russian-housing-market/data и расположить в папке data/raw

После этого можно открывать файл affection_of_features_preparation.ipynb и работать с ним в обычном режиме